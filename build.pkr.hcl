variable "acs_version" {
  type    = string
  default = "4.16.1.0"
}

variable "acs_version_suffix" {
  type    = string
}

source docker image {
  image  = "rockylinux:8.5"
  commit = true
}

build {
  source "docker.image" {
    changes = [
      "EXPOSE 8080 8443 8250 9090",
      "ENTRYPOINT [\"/docker-entrypoint.sh\"]",
      "CMD [\"acs-management\"]"
    ]
  }

  provisioner ansible {
    playbook_file = "${path.root}/ansible/main.yml"
    ansible_env_vars = [
      "ANSIBLE_HOST_KEY_CHECKING=False",
      "ANSIBLE_SSH_ARGS='-o HostKeyAlgorithms=+ssh-rsa -o PubkeyAcceptedKeyTypes=+ssh-rsa'"
    ]
    extra_arguments = [
      "--extra-vars",
      "acs_version=${var.acs_version}"
    ]
  }

  post-processors {
    post-processor docker-tag {
      repository = "registry.gitlab.com/phonexia.josef.vecera/acs-management"
      tags       = ["${var.acs_version}${var.acs_version_suffix}", "latest"]
    }

    post-processor docker-push {}
  }
}
